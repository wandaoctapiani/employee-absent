import React, { Component } from 'react';
import './App.css';
import 'materialize-css/dist/css/materialize.min.css';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import 'materialize-css/dist/css/materialize.min.css';
import ReasonPage from './components/ReasonPage/reasonpage.js';
import Date from './components/date';
import chart from './components/chart';
import absencePage from './components/AbsencePage/absencePage';
import absentPage from './components/absentPage';
import LatePage from './components/Late/latePage';
import byDate from "./components/byDate";
import test from './components/test';
import test2 from './components/test2';
import dashboard from './components/dashboard';

// be777827f7d0db07fa175236264c8a0e

class App extends Component{
  render(){
    return(
      <Router>
          <div>
              <Route exact path="/" component={test2}/>
              <Route exact path="/test" component={test}/>
              <Route exact path="/absencePage" component={absencePage}/>
              <Route exact path="/reasonpage" component={ReasonPage}/>
              <Route exact path="/date" component={Date}/>
              <Route exact path="/chart" component={chart}/>
              <Route exact path="/absentPage" component={absentPage}/>
              <Route exact path="/latePage" component={LatePage}/>
              <Route exact path="/dashboard" component={dashboard}/>
              <Route exact path="/byDate" component={byDate}/>
          </div>
      </Router>
    )
  }
}
export default App;