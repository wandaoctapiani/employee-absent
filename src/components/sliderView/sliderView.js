import React from 'react';
import SwipeableViews from 'react-swipeable-views';
import AbsencePage from "../AbsencePage/absencePage";
import AbsentPage from "../absentPage";
import LatePage from "../Late/latePage";
import ReasonPage from "../ReasonPage/reasonpage";
import { autoPlay } from 'react-swipeable-views-utils';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const styles = {
  slide: {
    padding: 15,
    minHeight: 100,
    color: '#000',
  },
  // slide1: {
  //   background: '#FEA900',
  // },
  slide2: {
    background: '#B3DC4A',
  },
  slide3: {
    background: '#6AC0FF',
  },
};

function MyComponent() {
  return (
    
    <AutoPlaySwipeableViews>
      <div style={Object.assign({}, styles.slide)}>
      <AbsencePage/>
      </div>
      <div style={Object.assign({}, styles.slide)}>
      <AbsentPage/>
      </div>
      <div style={Object.assign({}, styles.slide)}>
      <ReasonPage/>
      </div>
      <div style={Object.assign({}, styles.slide)}>
      <LatePage/>
      </div>
      </AutoPlaySwipeableViews>

    
  );
}

export default MyComponent;