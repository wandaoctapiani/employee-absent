import React, {Component} from "react"
import '../App.css'
import Logo from './logo'
import Keterangan from './keterangan'
import Title from './titleTracking'
import Date from './date'
import Absent from "./absent";

export default class absencePage extends Component {
    


  render(){
    return(
          <div>
         {/* employee Hadir   */}
      <div className="container">
        <div className="row">
            <Logo/>
            <Date/>
            <Title/>
            <Keterangan/>
            <Absent/>
        </div> 
      </div>
      {/* <marquee> Namun sebenarnya istilah ini sudah lama digunakan dalam dunia percetakan untuk menjelaskan secara detail mengenai penataan huruf.</marquee> */}
           </div>        
    );
  }
}
