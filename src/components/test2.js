import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from 'axios';
import Carousel from 'nuka-carousel';
import Peoples from './people';
import Logo from "./logo";
import Title from "./titleTracking";
  
class Home extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      dataNewRollout:[],
      dataFinance:[],
      dataTP:[],
      dataCM:[],
      dataSS: [],
      dataCR:[],
      dataTP:[],
      dataBD:[],
      Status: false,
      isLoading: true,
      errors: null
    };
  }

  componentDidMount(){
    this.handleNewRollout();
    this.handleSolutionSpecialist();
    this.handleFinance();
    this.handleCM();
    this.handleCR();
    this.handleTP();
    this.handleBD();
}
 
  async handleNewRollout(e){
    await axios.get('https://api2.groovy.id:3000/Department/New%20Rollout')
    .then(res => {
      let data = res.data
      this.setState({dataNewRollout : data,
        status : 0,
        isLoading: true
      })
      console.log(data)
      console.log(this.state.status)
    }).catch(console.log)
    if (this.state.Status === true) {
      console.log("tidak telat")
      // document.getElementById("ColorMerah").style.display = 'block';
      // document.getElementById("ColorHijay").style.display = "none";
    } else if(this.state.Status === "Telat") {
      console.log("telat")
      // document.getElementById("ColoHijau").style.display = "block"
      // document.getElementById("ColorMerah").style.display = "none"
    }
  }
 
  async handleSolutionSpecialist(){
   await axios.get('https://api2.groovy.id:3000/Department/Solution%20Specialist')
    .then(res => {
      let data = res.data
      this.setState({dataSS : data,
        isLoading: false
      })
      // console.log(data)
    }).catch(console.log)
  }
  async handleFinance(){
    await axios.get('https://api2.groovy.id:3000/Department/Finance')
    .then(res => {
      let data = res.data;
      this.setState({dataFinance : data,
        isLoading: false
      })
      // console.log(data)
    }).catch(console.log)
  } 
  async handleCM(){
    await axios.get('https://api2.groovy.id:3000/Department/Content%20&amp;%20Marketing')
    .then(res => {
      let data = res.data;
      this.setState({dataCM : data,
        isLoading: false
      })
      // console.log(data)
    }).catch(console.log)
  }
  async handleCR(){
    await axios.get('https://api2.groovy.id:3000/Department/Corporate%20Resource')
    .then(res => {
      let data = res.data;
      this.setState({dataCR : data,
        isLoading: false
      })
      // console.log(data)
    }).catch(console.log)
  }
  async handleTP(){
    await axios.get('https://api2.groovy.id:3000/Department/Technology%20&amp;%20Product')
    .then(res => {
      let data = res.data;
      this.setState({dataTP : data,
        isLoading: false
      })
      // console.log(data)
    }).catch(console.log)
  }
  async handleBD(){
    await axios.get('https://api2.groovy.id:3000/Department/Board%20of%20Director')
    .then(res => {
      let data = res.data;
      this.setState({dataBD : data,
        isLoading: false
      })
      // console.log(data)
    }).catch(console.log)
  } 
    render() {
      const settings = {
      dots: true,
      infinite: false,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1
    };
      const {isLoading, posts } = this.state;

      if(isLoading){
        return <p>Loading ...</p>
      }

        return (
          <div className="container">
          <Logo/>
          <Title/>
            <div className="row">
                <div className="">
                    <div className="col s6">
                            <h5>New Rollout</h5>
                            <div className="parent-card">
                                <Peoples peoples={this.state.dataNewRollout} />
                            </div>
                    </div>
                    <div className="col s6">
                            <h5>Finance</h5>
                            <div className="parent-card">
                                <Peoples peoples={this.state.dataFinance}/>
                            </div>
                    </div>
                    {/* <div className="col s6">
                            <h5>Content Marketing</h5>
                            <Peoples peoples={this.state.dataCM}/>
                     </div>      
                     <div className="col s6">
                            <h5>Solutions Specialist</h5>
                            <Peoples peoples={this.state.dataSS}/>
                     </div> */}
                </div>
                      {/* <div className="row">
                              <div className="col s6">
                                    <h5>Corporate Resource</h5>
                                    <div className="parent-card">
                                        <Peoples peoples={this.state.dataCR}/>
                                    </div>
                              </div>
                              <div className="col s6">
                                  <h5>Technology Product</h5>
                                  <div className="parent-card">
                                        <Peoples peoples={this.state.dataTP}/>
                                  </div>
                              </div>
                              <div className="col s6">
                                  <h5>Board of Director</h5>
                                  <div className="parent-card">
                                        <Peoples peoples={this.state.dataBD}/>
                                  </div>
                              </div>
                              <div className="col s6">
                                  <h5>Content Marketing</h5>
                                  <div className="parent-card">
                                        <Peoples peoples={this.state.dataCM}/>
                                  </div>
                              </div>
                              <div className="col s6">
                                  <h5>Solution Spesialist</h5>
                                  <div className="parent-card">
                                        <Peoples peoples={this.state.dataSS}/>
                                  </div>
                              </div>
                      </div> */}
                      
            </div>  
          </div>
          // <div className="container">
          //       <div className="row">
          //           <div className="col s6">
          //                   <h5>Content Marketing</h5>
          //                   <Peoples peoples={this.state.dataCM}/>
          //           </div>      
          //           <div className="col s6">
          //                   <h5>Solutions Specialist</h5>
          //                   <Peoples peoples={this.state.dataSS}/>
          //           </div>
          //       </div>
          // </div>
        );
 
    }
  }
export default Home;
