import React, {Component} from "react";
import '../../App.css';
import Logo from '../../components/logo';
import Keterangan from '../keterangan'
import CheckIn from '../checkIn'
import Title from '../titleTracking'
import Date from '../date'

export default class absencePage extends Component {
  

  render(){
    return(
          <div>
         {/* employee Hadir   */}
      <div className="container">
        <div className="row">
            <Logo/>
            <Date/>
            <Title/>
            <Keterangan/>
            <CheckIn/>
        </div> 
      </div>
      {/* <marquee> Namun sebenarnya istilah ini sudah lama digunakan dalam dunia percetakan untuk menjelaskan secara detail mengenai penataan huruf.</marquee> */}
           </div>        
    );
  }
}
