import React, { Component } from 'react';
import ReactMinimalPieChart from 'react-minimal-pie-chart';
import "../App.css";

class chart extends Component {
    render() {
        return (
            <div>
                <ReactMinimalPieChart className="chart"
                    data={[
                        {
                            title: 'One',
                            value: 10,
                            color: '#E38627'
                        },
                        {
                            title: 'Two',
                            value: 20,
                            color: '#C13C37'
                        }
                    ]}
                    animate
                    lineWidth={15}
                />
            </div>
        );
    }
}

export default chart;