import React from 'react'
import Carousel from 'nuka-carousel';


    const Peoples = ({ peoples, isLoading }) => {
      return (
          
               <div>
                 {/* <div className="BigBox">
                    <h5>Data Karyawan</h5>
                 </div> */}
                   <Carousel withoutControls={true} autoplay={3000}>
                                  
                   {!isLoading ?  (
                    peoples.map(people => {
                      
                    return (
                      <div className="">
                         <div>
                            {/* <img className="img-checkIn" src="https://img.icons8.com/windows/150/000000/user.png"/> */}
                                <h5 className="employee">{people.Name}</h5>
                                <h6 className="title">{people["First In"]}</h6>
                                <p className="card-text">{people.Status}</p>
                            
                        </div>
                        </div>   
                    );
                    })
                  ) : (
                       
                    <p>Loading ...</p>
                  )} 
                 
                    </Carousel>
                </div>
                
          
        
      )
    };

export default Peoples
