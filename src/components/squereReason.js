import React, { Component } from 'react';
import axios from "axios";
import Filters from "./filters";
import Carousel from 'nuka-carousel';

class squereReason extends Component {
    constructor(){
        super()
        this.state = {
            datapermit: [],
            dataleave: [],
            datasick: []
        };
    }
    componentDidMount(){
        this.handlePermit()
        this.handleSick()
        this.handleLeave()
    }
    handlePermit(){
        axios.get(`http://103.253.115.21:4000/Leave/Permit`)
        .then(res => {
            let data = res.data
            this.setState({
                datapermit : data
            });
            console.log(data)
        })
    }
    handleSick(){
        axios.get(`http://103.253.115.21:4000/Leave/Sick%20Leave`)
        .then(res => {
            let data = res.data
            this.setState({
                datasick : data
            });
            console.log(data)
        })
    }
    handleLeave(){
        axios.get(`http://103.253.115.21:4000/Leave/Annual%20Leave`)
        .then(res => {
            let data = res.data
            this.setState({
                dataleave : data
            });
            console.log(data)
        })
    }
    render() {
        const settings = {
            dots: true,
            infinite: false,
            speed: 1000,
            slidesToShow: 1,
            slidesToScroll: 1
        }

        return (
            <div>
                 <div className="content">
                            <div className="col s12">
                            <div className="col s4">
                                    
                                    <div className="card-reason">
                                        <h5 className="center">Sick</h5>
                                                <div className="card-sick center">
                                                    <Filters filters={this.state.datasick} />
                                                </div>
                                    </div>
                                    
                            </div>
                            <div className="col s4">
                                    <div className="card-reason">  
                                    <h5 className="center">Permit</h5>
                                            <div className="card-noinfo center">
                                                <Filters filters={this.state.datapermit} />
                                            </div>
                                        </div>
                            </div>
                            <div className="col s4">
                                    <div className="card-reason">  
                                    <h5 className="center">Leave</h5>
                                            <div className="card-leave center">
                                                <Filters filters={this.state.dataleave} />
                                            </div>
                                        </div>
                            </div>   
                            </div>  
                        </div>
            </div>
        );
    }
}

export default squereReason