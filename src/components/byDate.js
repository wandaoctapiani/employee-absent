import React, { Component } from 'react';
import { render } from 'react-dom';
import axios from "axios";
import sortBy from 'sort-by';
import Filters from "./filters";
import Carousel from 'nuka-carousel';
export default class App extends Component {
constructor(){
    super()
   this.state = {
    products:[],
    ownerName: [],
    dataPermit:[],
    dataFinance:[],
    dataCR:[],
    dataCM:[]
  };
}
  
  
  componentDidMount(){
      this.handleData()
      this.handleDataFinance()
      this.handleCM()
      this.handleDataCR()
  }
  
  handleData(){
      axios.get('http://103.253.115.21:4000/Department/New%20Rollout')
      .then(res => {
          let data = res.data;
          this.setState({
              dataPermit : data
          });
          console.log(data)
      })

  }

  handleDataFinance(){
    axios.get('http://103.253.115.21:4000/Department/Finance')
    .then(res => {
        let data = res.data;
        this.setState({
            dataFinance : data
        });
        console.log(data)
    })

}
handleDataCR(){
  axios.get('http://103.253.115.21:4000/Department/Corporate%20Resource')
  .then(res => {
      let data = res.data;
      this.setState({
          dataCR : data
      });
      console.log(data)
  })

}
handleCM(){
  axios.get('http://103.253.115.21:4000/Department/Content%20&amp;%20Marketing')
  .then(res => {
    let data = res.data;
    this.setState({dataCM : data,
      // isLoading: false
    })
    console.log(data)
  }).catch(console.log)
}
  render(){
      return(
          <div className="container">
              <div className="row">
                  <div className="col s6">
                      <h3>New Rollout</h3>
                      <Filters filters={this.state.dataPermit} />
                  </div>
                  <div className="col s6">
                        <h3>Content Marketing</h3>
                        <Filters filters={this.state.dataCM} />
                    </div>
                  <div className="col s6">
                      <h3>Corporate Resource</h3>
                      <Filters filters={this.state.dataCR} />
                  </div>
              {/* <div className="test">
                    
              </div> */}
              </div>
              
             
                {/* <button onClick={this.sortByPriceAsc}>
          ASC
        </button>
        <button onClick={this.sortByPriceDesc}>
          DESC
        </button>
        <div className="pro                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     duct">
      { this.state.products.map((product, index) =>
        <div key ={index} className="card">
          <h3>{ product.ownerName }</h3>
          <p>{ product.From }</p>
        </div>
      )}
      </div> */}
          </div>
      )
  }
}