import React, { Component } from "react";
import ReactDOM from "react-dom";
import axios from 'axios';
import '../App.css'
import { autoPlay } from 'react-swipeable-views-utils';
import SwipeableViews from 'react-swipeable-views';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);
  
class Home extends Component {
 
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      Empdata:[],
      isLoading: true,
      errors: null
    };
  }

  componentDidMount(){
    this.getPost()
  }
  
  getPost(){
    axios.get(`http://103.253.115.21:4000/Departments`)
    .then(res => {
      let data = res.data;
      this.setState({ posts : data,
        isLoading: false
      });
      console.log(data);

    })
    .catch(error => this.setState({error, isLoading: false}));
    
  }
  handleData(dept){
    axios.get(`http://103.253.115.21:4000/Department`, dept)
    .then(res => {
      let data = res.data;
      this.setState({ Empdata : data,
        isLoading: false
      });
      console.log(data);

    })
    .catch(error => this.setState({error, isLoading: false}));
    
  } 
    render() {
      
      const {isLoading, posts } = this.state;
        return (
         <div className="container">
            <h2>Check-In User</h2>
                <div className="row"> 
                <div className=""> 
                <AutoPlaySwipeableViews interval={3000}>
                
                  {!isLoading ?  (
                    posts.map(post => {
                      {this.handleData(post)}
                    return (
                          <div className="parent-card">
                                  <div className="title-depart">
                                     <div id="department">
                                        {post}
                                      </div>
                                  </div>
                          </div> 
                    );
                    })
                    
                  ) : (
                    <p>Loading ...</p>
                  )} 
                  </AutoPlaySwipeableViews>
                   </div>
                </div>
          </div>
          
        );
 
    }
  }
export default Home;