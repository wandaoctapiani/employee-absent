import React, {Component} from "react";
// import './style.css';
import axios from 'axios';
var moment = require('moment');


export default class Late extends Component {
    
  constructor(){
    super()
    this.state={
      datas: [],
      Names: [],
      depts: [],
      startTime: moment("01:00:00", "HH:mm:ss"),
      endTime : moment("08:00:00", "HH:mm:ss"),
    }
    
    }
  
    async handleData(){
      await axios.get('http://103.253.115.21:4000/people')
      .then(res => {
        let datas =  res.data;
        console.log(datas)
        
      })
      // console.log(this.state.datas)
    }
    // componentDidMount(dept){
    //   axios.get(`http://103.253.115.21:4000/Departments` + dept)
    //   .then(res => {
    //     const Names = res.data;
    //     this.setState({ Names });
    //     console.log(Names);
    //   })
    // }
    
    

  render(){
    return(
          <div>
            <div className="col s12">
            <div className="col s6 center">
            <div className="card-content">
                <img className="img-late" src="https://img.icons8.com/color/300/000000/user-male-skin-type-3.png"/>
                {/* employeeName and employeeDivisi */}
                <div className="employee">
                      <h3>Lenardi Ariyanto</h3>
                      <h5 className="title-depart">New Rollout</h5> 
                      <h5 className="color-late" onChange={this.handleData}></h5> 
                 </div>
            </div>
            </div>
            <div className="col s6 center">
            <div className="card-content">
                <img className="img-late" src="https://img.icons8.com/color/300/000000/user-female-skin-type-4.png"/>
                <div className="employee">
                        <h3>Wanda Oktapiani</h3>
                        <h5 className="title-depart">Tekhnology & Produk</h5> 
                        <h5 className="color-late"></h5> 
                   </div>
            </div>
           
            </div>
            </div>
      </div>        
    );
  }
}
