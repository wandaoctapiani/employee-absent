import React, { Component } from 'react';
import TitleReason from '../titleReason';
import Logo from "../logo";
import SquereReason from "../squereReason";
import Date from "../date";
import "../../App.css";

class reasonpage extends Component {
    render() {
        return (
            <div className="container">
                <div className="row">
                 <Logo/>
                 <Date/>
                <TitleReason/>
                <SquereReason/>
                </div>
               
            </div>
        );
    }
}

export default reasonpage