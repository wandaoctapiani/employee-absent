import React from 'react'
import Carousel from 'nuka-carousel';


    const Filters = ({ filters, isLoading }) => {
      return (
          
               <div className="parent-card">
                    <div className="child-card">
                 {/* <div className="BigBox">
                    <h5>Data Karyawan</h5>
                 </div> */}
                  <Carousel autoplay={true} autoplayInterval={3000} withoutControls={true}>    
                   {!isLoading ?  (
                    filters.map(filter => {
                    return (
                      
                         <div className="">
                            <div className="">
                            <img className="img-checkIn" src="https://img.icons8.com/windows/150/000000/user.png"/>
                                <h5 className="employee">{filter.Name}</h5>
                                <p className="card-text">{filter["First In"]}</p>
                                <h5 className="employee">{filter.Status}</h5>
                            </div>
                        </div>
                      
                    );
                    })
                  ) : (
                    <p>Loading ...</p>
                  )} 
                  </Carousel>
                  </div>
                </div>
          
        
      )
    };

export default Filters