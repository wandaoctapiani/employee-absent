import React, { Component } from 'react';


class titleReason extends Component {
    render() {
        return (
            <div>
                 <h3 className="title-employee">Employee Absent Reasons</h3>
                 <h5 className="title-city">Jakarta</h5>
            </div>
        );
    }
}

export default titleReason;

// import React, { Component } from "react";
// import ReactDOM from "react-dom";
// import axios from 'axios';
// import Peoples from './people';

  
// class Home extends Component {
 
//   constructor(props) {
//     super(props);
//     this.state = {
//       dataNewRollout:[],
//       dataFinance:[],
//       dataTP:[],
//       dataCM:[],
//       dataSS: [],
//       isLoading: true,
//       errors: null
//     };
//   }

//   componentDidMount(){
//     this.handleNewRollout();
//     this.handleSolutionSpecialist();
//     this.handleFinance();
//     this.handleCM();
// }
 
//   handleNewRollout(){
//     axios.get('http://103.253.115.21:4000/Department/New%20Rollout')
//     .then(res => {
//       let data = res.data;
//       this.setState({dataNewRollout : data,
//         isLoading: true
//       })
//       console.log(data)
//     }).catch(console.log)
//   }
 
//   handleSolutionSpecialist(){
//     axios.get('http://103.253.115.21:4000/Department/Solution%20Specialist')
//     .then(res => {
//       let data = res.data;
//       this.setState({dataSS : data,
//         isLoading: false
//       })
//       console.log(data)
//     }).catch(console.log)
//   }
//   handleFinance(){
//     axios.get('http://103.253.115.21:4000/Department/Finance')
//     .then(res => {
//       let data = res.data;
//       this.setState({dataFinance : data,
//         isLoading: false
//       })
//       console.log(data)
//     }).catch(console.log)
//   }
//   handleCM(){
//     axios.get('http://103.253.115.21:4000/Department/Content%20&amp;%20Marketing')
//     .then(res => {
//       let data = res.data;
//       this.setState({dataCM : data,
//         isLoading: false
//       })
//       console.log(data)
//     }).catch(console.log)
//   } 
//     render() {
//       const settings = {
//       dots: true,
//       infinite: false,
//       speed: 1000,
//       slidesToShow: 1,
//       slidesToScroll: 1
//     };
//       const {isLoading, posts } = this.state;

//       if(isLoading){
//         return <p>Loading ...</p>
//       }

//         return (
//          <div className="container">
//               <h2>New Rollout</h2>
//                 <Peoples peoples={this.state.dataNewRollout} />
//               <h2>Solutions Specialist</h2>
//                 <Peoples peoples={this.state.dataSS}/>
//               <h2>Finance</h2>
//                 <Peoples peoples={this.state.dataFinance}/>
//               <h2>Content Marketing</h2>
//                 <Peoples peoples={this.state.dataCM}/>
//           </div>
          
//         );
 
//     }
//   }
// export default Home;