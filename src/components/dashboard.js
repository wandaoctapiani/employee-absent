import React, {Component} from 'react';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import axios from 'axios';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const styles = {
    slide: {
      padding: 15,
      minHeight: 100,
      color: '#fff',
    },
    slide1: {
      background: '#FEA900',
    },
    slide2: {
      background: '#B3DC4A',
    },
    slide3: {
      background: '#6AC0FF',
    },
  };
   
class Home extends Component{
    constructor(props) {
        super(props);
        this.state = {
          posts: ["New Rollout","Solution Specialist","Technology &amp; Product","Content &amp; Marketing","Corporate Resource","Finance","Board of Director"]          ,
          empData:[],
          isLoading: true,
          errors: null
        }
      }
      componentDidMount(){
          this.getPost()
      }
     
      getPost(){
        axios.get(`http://103.253.115.21:4000/Departments`)
        .then(res => {
          let data = res.data;
          this.setState({ posts : data,
            isLoading: false
          });
          console.log(data);
    
        })
        .catch(error => this.setState({error, isLoading: false}));
        
      }
      handleData(dept){
         axios.get('http://103.253.115.21:4000/Department/'+ dept)
        .then(res =>{
            let data = res.data;
            this.setState({
                empData: data,
                isLoading: false
            })
        })
      }
    render(){
        const {isLoading, posts, empData } = this.state;
        return(
           <div>
                <div className="container">
                <h2>Check In User</h2>
                <div className="row">
                    <AutoPlaySwipeableViews interval={3000}>
                        {!isLoading ?  (
                            posts.map(post => {
                              
                                return (
                                    <div className="parent-card">
                                        <div className="title-depart">
                                            <div id="department">
                                                {post}
                                                <AutoPlaySwipeableViews interval={10000}>
                                                {this.handleData(post)}
                                                    {!isLoading ? (
                                                        empData.map(x =>{
                                                            return(
                                                                <div className="child-card">{x.Name}</div>
                                                            )
                                                        })
                                                    ): (
                                                        <p>Loading ...</p>
                                                        )} 
                                                    
                                                     {/* <div>test</div>
                                                     <div>test 2</div>
                                                     <div>test 3</div> */}
                                                </AutoPlaySwipeableViews>

                                            </div>
                                        </div>
                                    </div> 
                                    );
                                })
                            ) : (
                        <p>Loading ...</p>
                        )} 

                        {/* <div className="parent-card">
                        Test 2
                            <AutoPlaySwipeableViews interval={2000}>
                                <div style={Object.assign({}, styles.slide, styles.slide1)}>
                                slide n°1   
                                </div>
                                <div style={Object.assign({}, styles.slide, styles.slide2)}>
                                slide n°2
                                </div>
                                <div style={Object.assign({}, styles.slide, styles.slide3)}>
                                slide n°3
                                </div>
                            </AutoPlaySwipeableViews>
                        </div> */}
                    </AutoPlaySwipeableViews>
                </div>
            </div>
           </div>
        )
    }
}


export default Home;