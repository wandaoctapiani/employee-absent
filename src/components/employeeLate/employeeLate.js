import React, { Component } from 'react';
import Logo from '../../components/logo'
import './style.css'

class reasonpage extends Component {

    constructor(){
        super()
        this.state={time: new Date()}
    }

    render() {
        return (
            <div>
                <div className="container">
                    <div className="row">
                        <Logo/>
                        <h1 className="title">{this.state.time.toLocaleDateString()}</h1>
                       
                        <div className="content">
                            <div className="col s12">
                            <div className="col s4">
                                    <div className="card">
                                        <h5 className="center">Sick</h5>
                                                <div className="card-sick center">
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                    <h5>Wanda Oktapiani</h5>
                                                </div>
                                    </div>
                            </div>
                            <div className="col s4">
                                    <div className="card">  
                                    <h5 className="center">No Information</h5>
                                            <div className="card-noinfo center">
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            </div>
                                        </div>
                            </div>
                            <div className="col s4">
                                    <div className="card">  
                                    <h5 className="center">Leave</h5>
                                            <div className="card-leave center">
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            <h5>Herlinda Rosa</h5>
                                            </div>
                                        </div>
                            </div>   
                            </div>  
                        </div>
                       
                    </div>
                </div>
            </div>
        );
    }
}


export default reasonpage;